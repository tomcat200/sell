package com.imooc.dao;

import com.imooc.entity.OrderMaster;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderMasterMapperTest {

    @Autowired
    private OrderMasterMapper orderMasterMapper;
    @Test
    public void findByBuyerOpenid() {
        PageRequest pageRequest = new PageRequest(0, 1);
        Page<OrderMaster> result = orderMasterMapper.findByBuyerOpenid("110110", pageRequest);
        System.err.println(result.getTotalElements());
    }

    @Test
    public void save() {
        OrderMaster orderMaster = new OrderMaster();
        orderMaster.setOrderId("123457");
        orderMaster.setBuyerName("小师兄");
        orderMaster.setBuyerPhone("1234567879");
        orderMaster.setBuyerAddress("上海");
        orderMaster.setBuyerOpenid("120110");
        orderMaster.setOrderAmount(new BigDecimal(2.5));
        OrderMaster result = orderMasterMapper.save(orderMaster);
        Assert.assertNotNull(result);


    }
}