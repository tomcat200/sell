package com.imooc.dao;

import com.imooc.entity.OrderMaster;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author 光谷⚔神祈
 * @create 2019年02月21日 16:08
 * @target WORKING HARD,MARRY TO HER!
 * @import ★FAMILY★DREAM★
 **/
public interface OrderMasterMapper extends JpaRepository<OrderMaster,String> {

    Page<OrderMaster> findByBuyerOpenid(String buyerOpenid, Pageable pageable);
}
