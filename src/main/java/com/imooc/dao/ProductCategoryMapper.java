package com.imooc.dao;

import com.imooc.entity.ProductCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author 光谷⚔神祈
 * @create 2019年02月17日 23:13
 * @target WORKING HARD,MARRY TO HER!
 * @import ★FAMILY★DREAM★
 **/
public interface ProductCategoryMapper extends JpaRepository<ProductCategory,Integer> {

    List<ProductCategory> findByCategoryTypeIn(List<Integer> categoryTypeList);
}
