package com.imooc.dao;

import com.imooc.entity.ProductInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author 光谷⚔神祈
 * @create 2019年02月20日 15:54
 * @target WORKING HARD,MARRY TO HER!
 * @import ★FAMILY★DREAM★
 **/
public interface ProductInfoMapper extends JpaRepository<ProductInfo,String> {

    List<ProductInfo> findByProductStatus(Integer productStatus);
}
