package com.imooc.dao;

import com.imooc.entity.OrderDetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author 光谷⚔神祈
 * @create 2019年02月21日 16:11
 * @target WORKING HARD,MARRY TO HER!
 * @import ★FAMILY★DREAM★
 **/
public interface OrderDetailMapper extends JpaRepository<OrderDetail,String> {
    List<OrderDetail> findByOrderId(String orderId);
}
