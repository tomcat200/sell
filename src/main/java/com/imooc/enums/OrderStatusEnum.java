package com.imooc.enums;

import lombok.Getter;

/**
 * @author Mercedes-X-Phoenix
 * @create 2019年02月21日 15:41
 * @target Working Hard,Marry To Her!
 * @import ★FAMILY★DREAM★
 **/
@Getter
public enum OrderStatusEnum {
    NEW(0,"新订单"),
    FINISHED(1,"完结"),
    CANCEL(2,"已取消")
    ;


    private Integer code;
    private String message;

    OrderStatusEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}