package com.imooc.utils;

import com.imooc.VO.ResultVO;

/**
 * @author Mercedes-X-Phoenix
 * @create 2019年02月21日 11:56
 * @target Working Hard,Marry To Her!
 * @import ★FAMILY★DREAM★
 **/
public class ResultVOUtil {
    public static ResultVO success(Object object) {
        ResultVO resultVO = new ResultVO();
        resultVO.setData(object);
        resultVO.setCode(0);
        resultVO.setMessage("成功");
        return resultVO;
    }

    public static ResultVO success() {
        return success(null);
    }

    public static ResultVO error(Integer code, String message) {
        ResultVO resultVO = new ResultVO();
        resultVO.setCode(code);
        resultVO.setMessage(message);
        return resultVO;
    }
}
