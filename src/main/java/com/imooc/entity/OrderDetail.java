package com.imooc.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;

/**
 * @author Mercedes-X-Phoenix
 * @create 2019年02月21日 15:55
 * @target Working Hard,Marry To Her!
 * @import ★FAMILY★DREAM★
 **/
@Entity
@Data
public class OrderDetail {
    @Id
    private String detailId;
    private String orderId;
    private String productId;
    private String productName;
    private BigDecimal productPrice;
    private Integer productQuantity;
    private String productIcon;


}
