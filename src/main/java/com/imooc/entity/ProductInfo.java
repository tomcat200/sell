package com.imooc.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;

/**
 * 商品信息表
 *
 * @author Mercedes-X-Phoenix
 * @create 2019年02月18日 16:51
 * @target Working Hard,Marry To Her!
 * @import ★FAMILY★DREAM★
 **/
@Entity
@Data
public class ProductInfo {


    @Id
    private String productId;

    //商品名字
    private String productName;
    //商品单价
    private BigDecimal productPrice;
    //商品库存
    private Integer productStock;
    //商品描述
    private String productDescription;
    //商品小图
    private String productIcon;
    //商品状态,0正常1下架
    private Integer productStatus;
    //商品类目
    private Integer categoryType;
}
