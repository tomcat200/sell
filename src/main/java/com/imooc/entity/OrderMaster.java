package com.imooc.entity;

import com.imooc.enums.OrderStatusEnum;
import com.imooc.enums.PayStatusEnum;
import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Mercedes-X-Phoenix
 * @create 2019年02月21日 15:38
 * @target Working Hard,Marry To Her!
 * @import ★FAMILY★DREAM★
 **/
@Entity
@Data
@DynamicUpdate
public class OrderMaster {

    @Id
    private String orderId;
    private String buyerName;
    private String buyerPhone;
    private String buyerAddress;
    private String buyerOpenid;
    private BigDecimal orderAmount;
    //订单状态,默认为新下单
    private Integer orderStatus=OrderStatusEnum.NEW.getCode();

    //支付状态,默认为0未支付
    private Integer payStatus=PayStatusEnum.WAIT.getCode();

    private Date createTime;

    private Date updateTime;

}
