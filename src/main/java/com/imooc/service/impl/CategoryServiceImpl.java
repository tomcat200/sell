package com.imooc.service.impl;

import com.imooc.dao.ProductCategoryMapper;
import com.imooc.entity.ProductCategory;
import com.imooc.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Mercedes-X-Phoenix
 * @create 2019年02月18日 14:46
 * @target Working Hard,Marry To Her!
 * @import ★FAMILY★DREAM★
 **/
@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    private ProductCategoryMapper productCategoryMapper;
    @Override
    public ProductCategory findOne(Integer categoryId) {
        return productCategoryMapper.findOne(categoryId);
    }

    @Override
    public List<ProductCategory> findAll() {
        return productCategoryMapper.findAll();
    }

    @Override
    public List<ProductCategory> findByCategoryTypeIn(List<Integer> categoryTypeList) {
        return productCategoryMapper.findByCategoryTypeIn(categoryTypeList);
    }

    @Override
    public ProductCategory save(ProductCategory productCategory) {
        return productCategoryMapper.save(productCategory);
    }
}
