package com.imooc.service;

import com.imooc.entity.ProductCategory;

import java.util.List;

/**
 * @author 光谷⚔神祈
 * @create 2019年02月18日 14:31
 * @target WORKING HARD,MARRY TO HER!
 * @import ★FAMILY★DREAM★
 **/
public interface CategoryService {

    //根据id查一条数据
    ProductCategory findOne(Integer categoryId);

    //查所有
    List<ProductCategory> findAll();

    //根据产品类目type查
    List<ProductCategory> findByCategoryTypeIn(List<Integer> categoryTypeList);

    //新增和更新
    ProductCategory save(ProductCategory productCategory);
}
