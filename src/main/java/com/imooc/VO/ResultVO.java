package com.imooc.VO;

import lombok.Data;

/**
 * http请求返回的最外层对象
 *
 * @author Mercedes-X-Phoenix
 * @create 2019年02月20日 17:10
 * @target Working Hard,Marry To Her!
 * @import ★FAMILY★DREAM★
 **/
@Data
public class ResultVO<T> {

    /**错误码 */
    private Integer code;
    /**提示信息 */
    private String message;
    /**返回的具体内容 */
    private T data;
}
